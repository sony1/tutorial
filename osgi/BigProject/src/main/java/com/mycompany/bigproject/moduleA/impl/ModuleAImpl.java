package com.mycompany.bigproject.moduleA.impl;

import com.mycompany.bigproject.moduleA.ModuleA;

/**
 * Created by fani on 12/19/14.
 */
public class ModuleAImpl implements ModuleA {

    @Override
    public void methodPadaModuleA() {
        System.out.println("\n\nHalo... saya method pada module A\n\n");
    }

}
