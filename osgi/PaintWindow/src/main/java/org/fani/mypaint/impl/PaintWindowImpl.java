package org.fani.mypaint.impl;

import org.fani.mypaint.PaintWindow;
import org.fani.mypaint.painter.api.Painter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

public class PaintWindowImpl
        extends JFrame
        implements PaintWindow, ActionListener, MouseListener {

    Map<JButton, Painter> regs;
    JToolBar toolBar;
    JButton selected;

    public PaintWindowImpl() {
        super();
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        regs = new HashMap<JButton, Painter>();

        initComponents();
    }

    private void initComponents() {
        toolBar = new JToolBar("Toolbar");

        setPreferredSize(new Dimension(500, 500));
        add(toolBar, BorderLayout.PAGE_START);
        add(new JPanel(), BorderLayout.CENTER);

        addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        JButton source = (JButton) actionEvent.getSource();
        selected = source;

//        regs.get(selected).draw(
//                getGraphics(), 100, 100);
    }

    @Override
    public void registerPainter(Painter painter) {
        JButton toolbarButton = painter.createButton();
        System.out.println("Regpainter: After create button");

        toolbarButton.addActionListener(this);

        regs.put(toolbarButton, painter);
        toolBar.add(toolbarButton);

        selected = toolbarButton;
    }

    @Override
    public void unregisterPainter(Painter painter) {
        int count = toolBar.getComponentCount();
        for (int i = 0; i < count; ++i) {
            for (Map.Entry<JButton, Painter> entry : regs.entrySet()) {
                JButton button = (JButton) toolBar.getComponent(i);
                if (button == entry.getKey()) {
                    System.out.println("found comp");
                    toolBar.remove(i);
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
//        System.out.println("selected = " + selected);
        if (selected != null) {
            regs.get(selected).draw(
                    getGraphics(),
                    mouseEvent.getX(),
                    mouseEvent.getY());
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
