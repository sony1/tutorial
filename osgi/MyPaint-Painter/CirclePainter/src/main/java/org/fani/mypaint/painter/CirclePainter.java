package org.fani.mypaint.painter;

import java.awt.*;

public class CirclePainter extends AbstractPainter {

    @Override
    public String getImageLocation() {
        return "/images/circle.png";
    }

    @Override
    public void draw(Graphics g, int x, int y) {
        super.drawImage(g, x, y);
    }

    @Override
    public Class getClassForResource() {
        return CirclePainter.class;
    }

}
