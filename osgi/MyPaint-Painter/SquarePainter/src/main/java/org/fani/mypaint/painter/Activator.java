package org.fani.mypaint.painter;

import org.fani.mypaint.PaintWindow;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        ServiceReference ref = bundleContext.getServiceReference(PaintWindow.class.getName());
        PaintWindow window = (PaintWindow) bundleContext.getService(ref);
        window.registerPainter(new SquarePainter());
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        //---
    }

}
