package com.nostratech.tutorial.persistence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="USER_CATEGORY")
public class UserCategory extends Base {
	
	@Column(name = "NAME")
    private String name;
	
	public UserCategory() {
	
	}
	
	public UserCategory(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCategory other = (UserCategory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
    
}
