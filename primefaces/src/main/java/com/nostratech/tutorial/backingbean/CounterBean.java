package com.nostratech.tutorial.backingbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by aguswinarno on 12/2/14.
 */

@ManagedBean(name = "counterBean")
@ViewScoped
public class CounterBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void increment() {
        count++;
    }
}